# PHP Resource Wrapper
Bunch of classes defining an API for classes that embed resources.

## What is the purpose?
This project itself is not useful if you are not implementing your own logic to it.
If you are using it raw, you can see that the classes are all abstracts.
However, you can use it using Composer with the command
```bash
composer require jumpifbelow/php-resource-wrapper
```

If you want an implementation of it, there is actually one that is usable with
```bash
composer require jumpifbelow/php-resource-class
```
This project is accessible with [this link](https://github.com/JumpIfBelow/php-resource-class).

Actually, those two projects are decoupled to allow major update on one part, without having to implement all new features immediatly.

## Why those two projects are split?
The main goal is to be able to update one without crashing another.
For example, if one day I create a brand new version of the API, the dependency will ensure 
that the implementation works, without having to update all of the code.
Moreover, it allows people to use it for their own purpose and maybe enhance what I've done
with this project. I hope so :)
