<?php

namespace ResourceWrapper;

/**
 * Class CloseableAbstractResourceWrapper
 * @package ResourceWrapper\Model
 */
abstract class AbstractCloseableResourceWrapper extends AbstractResourceWrapper
{
    /**
     * @inheritdoc
     */
    public function __sleep()
    {
        $properties = parent::__sleep();

        // prevents the resource closed before calling serialize()
        if ($this->isResource()) {
            $this->close();
        }

        return $properties;
    }

    /**
     * Function to cleanly destruct the resource
     */
    public function __destruct()
    {
        if ($this->isResource()) {
            $this->close();
        }
    }

    /**
     * Close the resource wrapper
     * @return bool True on success, false otherwise
     */
    abstract public function close(): bool;
}