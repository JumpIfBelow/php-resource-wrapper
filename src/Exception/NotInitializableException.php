<?php

namespace ResourceWrapper\Exception;

use Exception;

/**
 * Class NotInitializableException
 * This exception is used when the resource cannot be initialized.
 * @package ResourceWrapper\Exception
 */
class NotInitializableException extends Exception
{
}
