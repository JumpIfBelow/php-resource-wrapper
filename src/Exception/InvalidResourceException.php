<?php

namespace ResourceWrapper\Exception;

use Exception;

/**
 * Class InvalidResourceException
 * This exception is used when a given resource is invalid
 * @package ResourceWrapper\Model
 */
class InvalidResourceException extends Exception
{
}