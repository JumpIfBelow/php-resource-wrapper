<?php

namespace ResourceWrapper;

use InvalidArgumentException;
use ResourceWrapper\Exception\InvalidResourceException;
use ResourceWrapper\Exception\NotInitializableException;

/**
 * Class ResourceWrapper
 * @package ResourceWrapper\Model
 * @link http://php.net/manual/en/resource.php
 */
abstract class AbstractResourceWrapper
{
	const ACCEPT_ALL = 0;

	/**
	 * @var callable|null $resourceCallable
	 */
	protected $resourceCallable;

	/**
	 * @var array $resourceParameters
	 */
	protected $resourceParameters;

	/**
	 * @var resource|null $resource
	 */
	private $resource;

    /**
     * ClassResource constructor.
     * @param resource|null $resource
     * @throws InvalidResourceException
     * @internal Should not be used to create an instance of any of ResourceWrapper's child by the user.
     * Only ResourceWrapper's child itself can create an instance of itself.
     * It is not set as protected to allow user to create an instance from an existing resource, even if it is strongly discouraged.
     * If you want to convert an existing ResourceWrapper object, you better use {@see ResourceWrapper::convertTo()} method.
     */
	public function __construct($resource = null)
	{
		$this
			->setResource($resource)
			->setResourceCallable(null)
			->setResourceParameters([])
        ;
	}

	/**
	 * Returns accepted resources for the resource wrapper
	 * @return string[] The list of resources accepted by the resource wrapper
	 */
	abstract protected static function getAcceptedResources(): array;

    /**
     * Quick method to init resource and keep how to do so.
     * Enables to serialize/unserialize and clone it.
     * @param callable $resourceCallable
     * @param array $resourceParameters
     * @return static
     * @throws InvalidResourceException
     */
	protected static function initResource(callable $resourceCallable, array $resourceParameters = []): self
	{
		$resourceWrapper = new static();

		$resourceWrapper
			->setResourceCallable($resourceCallable)
			->setResourceParameters($resourceParameters)
			->createResource()
        ;

		return $resourceWrapper;
	}

	/**
	 * Calls directly the callable and automatically converts the parameters.
	 * @param callable $callable A resource-related callable, like fopen.
	 * @param array $parameters The parameters to set in.
	 * @return mixed The return value of $callable
	 */
	protected static function staticCall(callable $callable, array $parameters = [])
	{
		return $callable(...static::convertParameters($parameters));
	}

	/**
	 * Converts parameters that are instance of AbstractResourceWrapper to get the resource instead of object.
	 * @param array $parameters The parameters to convert.
	 * @return array The converted parameters.
	 */
	private static function convertParameters(array $parameters): array
	{
		$converted = [];

		foreach ($parameters as $parameter) {
			$converted[] = $parameter instanceof self ? $parameter->getResource() : $parameter;
		}

		return $converted;
	}

	/**
	 * Returns the resource
	 * @return resource|null
	 */
	public function getResource()
	{
		return $this->resource;
	}

	/**
	 * Set the resource for the resource wrapper
	 * @param resource|null $resource The resource to set
	 * @return $this
	 * @throws InvalidResourceException If the given resource does not match what {@see AbstractResourceWrapper::getAcceptedResources()} give
	 */
	public function setResource($resource): self
	{
		if ($resource !== null) {
			if (is_resource($resource)) {
				$resourceType = get_resource_type($resource);
				$acceptedResources = static::getAcceptedResources();

				if (!in_array($resourceType, $acceptedResources, true) && !in_array(static::ACCEPT_ALL, $acceptedResources, true)) {
					throw new InvalidResourceException('The accepted resources does not accept the "' . $resourceType . '" resource type.');
				}
			} else {
				throw new InvalidArgumentException('The $resource variable must be either a valid resource or null.');
			}
		}

		$this->resource = $resource;

		return $this;
	}

	/**
	 * Get the callable to init back the resource
	 * @return callable|null
	 */
	public function getResourceCallable()
	{
		return $this->resourceCallable;
	}

	/**
	 * Set the callable to init back the resource
	 * @param callable|null $resourceCallable
	 * @return $this
	 */
	protected function setResourceCallable(callable $resourceCallable = null): self
	{
		$this->resourceCallable = $resourceCallable;

		return $this;
	}

	/**
	 * Get the parameters used to init the resource
	 * @return array The init parameters
	 */
	public function getResourceParameters(): array
	{
		return $this->resourceParameters;
	}

	/**
	 * Set the parameters used to init the resource
	 * @param array $resourceParameters The init parameters
	 * @return $this
	 */
	protected function setResourceParameters(array $resourceParameters): self
	{
		$this->resourceParameters = $resourceParameters;

		return $this;
	}

	/**
	 * Check if the the resource wrapper is a resource
	 * @return bool True if it is resource, false otherwise
	 */
	public function isResource(): bool
	{
		return is_resource($this->getResource());
	}

	/**
	 * Returns the resource type
	 * @return string|null The type of the resource or null if not a resource
	 */
	public function getResourceType()
	{
		return $this->isResource() ? get_resource_type($this->getResource()) : null;
	}

	/**
	 * Converts a child of ResourceWrapper into another child of ResourceWrapper.
	 * @param string $className
	 * @return self A new instance of ResourceWrapper of the type set by $className.
	 */
	public function convertTo(string $className): self
	{
		if (!is_subclass_of($className, self::class)) {
			throw new InvalidArgumentException('The class must extends "' . self::class . '".');
		}

		if ($className === static::class) {
			throw new InvalidArgumentException('Can only convert into another type of ResourceWrapper.');
		}

		return new $className($this->getResource());
	}

	/**
	 * Function used to sleep the resource
     * @throws NotInitializableException
	 */
	public function __sleep()
	{
		if (!$this->isInitializable()) {
			throw new NotInitializableException('Cannot sleep the object as resource cannot be initialized back.');
		}

		return [
			'resourceCallable',
			'resourceParameters',
		];
	}

    /**
     * Function used to init back the resource
     * @throws NotInitializableException
     * @throws InvalidResourceException
     */
	public function __wakeup()
	{
		$this->createResource();
	}

    /**
     * Function used to clone a resource
     * @throws NotInitializableException
     * @throws InvalidResourceException
     */
	public function __clone()
	{
		$this->createResource();
	}

	/**
	 * Returns if the resource can be instanced or not
	 * @return bool True if the resource can be created, false otherwise
	 */
	public function isInitializable()
	{
		if (!is_callable($this->getResourceCallable()) || !is_array($this->getResourceParameters())) {
			return false;
		}

		foreach ($this->getResourceParameters() as $resourceParameter) {
			if ($resourceParameter instanceof self && !$resourceParameter->isInitializable()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Calls directly the callable and automatically converts the parameters. Set the $this resource in given $resourcePosition.
	 * @param callable $callable A resource-related callable, like fopen.
	 * @param array $parameters The parameters to set in.
	 * @param int $resourcePosition The resource position in the called function.
	 * @return mixed The return value of $callable
	 * @see AbstractResourceWrapper::staticCall()
     * @see AbstractResourceWrapper::compileParameters()
	 */
	protected function dynamicCall(callable $callable, array $parameters = [], int $resourcePosition = 0)
	{
		return static::staticCall($callable, $this->compileParameters($parameters, $resourcePosition));
	}

	/**
	 * Compiles parameter into array with $this inside.
	 * @param array $parameters The parameters to compile.
	 * @param int $resourcePosition The resource position in the outputed array.
	 * @return array
	 */
	protected function compileParameters(array $parameters = [], int $resourcePosition = 0): array
	{
		return array_merge(
			array_slice($parameters, 0, $resourcePosition),
			[$this],
			array_slice($parameters, $resourcePosition)
		);
	}

    /**
     * Quickly creates the resource from callable and parameters
     * @throws NotInitializableException
     * @throws InvalidResourceException
     */
	private function createResource()
	{
		if ($this->isInitializable()) {
			$resource = static::staticCall($this->getResourceCallable(), static::convertParameters($this->getResourceParameters()));
			$this->setResource($resource ?: null);
		} else {
			throw new NotInitializableException('Unable to initialize a new instance of the resource.');
		}
	}
}
