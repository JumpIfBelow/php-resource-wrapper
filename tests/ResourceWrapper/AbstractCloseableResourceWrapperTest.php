<?php

namespace Tests\ResourceWrapper;

use PHPUnit\Framework\TestCase;
use ResourceWrapper\AbstractCloseableResourceWrapper;

class AbstractCloseableResourceWrapperTest extends TestCase
{
	/**
	 * @see AbstractCloseableResourceWrapper::__sleep()
	 */
	public function test__sleep()
	{
        $rw = CloseableResourceWrapper::open(__FILE__, 'r');
        $s = serialize($rw);
        /** @var CloseableResourceWrapper $rw2 */
        $rw2 = unserialize($s);
        $this->assertInstanceOf(CloseableResourceWrapper::class, $rw2);
	}

	/**
	 * @see AbstractCloseableResourceWrapper::__destruct()
	 */
	public function test__destruct()
	{
        $rw = CloseableResourceWrapper::open(__FILE__, 'r');
        $r = $rw->getResource();
        $this->assertTrue(is_resource($r));
        unset($rw);
        $this->assertFalse(is_resource($r));
	}

	/**
	 * @see AbstractCloseableResourceWrapper::close()
	 */
	public function testClose()
	{
        $rw = CloseableResourceWrapper::open(__FILE__, 'r');
        $r = $rw->getResource();
        $this->assertTrue(is_resource($r));
        $rw->close();
        $this->assertFalse(is_resource($r));
	}
}

class CloseableResourceWrapper extends AbstractCloseableResourceWrapper
{
	/**
	 * @inheritdoc
	 */
	public function close(): bool
	{
		return fclose($this->getResource());
	}

	/**
	 * @inheritdoc
	 */
	protected static function getAcceptedResources(): array
	{
		return [
		    'stream',
        ];
	}

    /**
     * @param $_filename
     * @param $_mode
     * @param null $_useIncludePath
     * @param null $_context
     * @return \ResourceWrapper\AbstractResourceWrapper|static
     */
	public static function open($_filename, $_mode, $_useIncludePath = null, $_context = null)
    {
        return self::initResource('fopen', func_get_args());
    }
}
