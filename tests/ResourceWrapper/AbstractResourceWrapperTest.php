<?php

namespace Tests\ResourceWrapper;

use PHPUnit\Framework\TestCase;
use ResourceWrapper\AbstractResourceWrapper;
use ReflectionClass;

/**
 * Class AbstractResourceWrapperTest
 * @package Test\ResourceWrapper
 * @see AbstractResourceWrapper
 */
class AbstractResourceWrapperTest extends TestCase
{
	/**
	 * @see AbstractResourceWrapper::__construct()
	 */
	public function test__construct()
	{
        $r = fopen(__FILE__, 'r');
        $rw = new ResourceWrapper($r);
        $this->assertEquals($r, $rw->getResource());
        $this->assertEquals(AbstractResourceWrapper::class, get_parent_class($rw));
	}

	/**
	 * @see AbstractResourceWrapper::getAcceptedResources()
	 */
	public function testGetAcceptedResource()
	{
		$rc = new ReflectionClass(ResourceWrapper::class);
		$rm = $rc->getMethod('getAcceptedResources');
		$rm->setAccessible(true);
		$this->assertIsArray($rm->invoke(null));
	}

	/**
	 * @see AbstractResourceWrapper::initResource()
	 */
	public function testInitResource()
	{
		$rc = new ReflectionClass(ResourceWrapper::class);
		$rm = $rc->getMethod('initResource');
		$rm->setAccessible(true);

		$callable = 'fopen';
		$parameters = [
			__FILE__,
			'r',
		];

		/** @var AbstractResourceWrapper $resourceWrapper */
		$resourceWrapper = $rm->invoke(null, $callable, $parameters);
		$this->assertEquals(ResourceWrapper::class, get_class($resourceWrapper));
		$this->assertEquals($callable, $resourceWrapper->getResourceCallable());
		$this->assertEquals($parameters, $resourceWrapper->getResourceParameters());
	}

	/**
	 * @see AbstractResourceWrapper::getResource()
	 */
	public function testGetResource()
	{
		$r = fopen(__FILE__, 'r');
		$resourceWrapper = new ResourceWrapper($r);
		$this->assertEquals($r, $resourceWrapper->getResource());
	}

	/**
	 * @see AbstractResourceWrapper::setResource()
	 */
	public function testSetResource()
	{
        $r = new ResourceWrapper();
        $r->setResource(STDIN);

        $this->assertEquals($r->getResource(), STDIN);
	}

	/**
	 * @see AbstractResourceWrapper::isResource()
	 */
	public function testIsResource()
	{
        $r = new ResourceWrapper();
        $this->assertEquals(false, $r->isResource());

        $r->setResource(STDIN);
        $this->assertEquals(true, $r->isResource());
	}

	/**
	 * @see AbstractResourceWrapper::getResourceType()
	 */
	public function testGetResourceType()
	{
        $r = new ResourceWrapper(STDIN);
        $this->assertEquals('stream', $r->getResourceType());
	}

	/**
	 * @see AbstractResourceWrapper::isInitializable()
	 */
	public function testIsInitializable()
	{
        $r = new ResourceWrapper();

        $this->assertEquals(false, $r->isInitializable());
	}
}

class ResourceWrapper extends AbstractResourceWrapper
{
	protected static function getAcceptedResources(): array
	{
		return [
			self::ACCEPT_ALL,
		];
	}
}
